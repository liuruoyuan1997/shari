package models

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var db *mongo.Database

func init() {
	//e := godotenv.Load() //Load .env file
	//if e != nil {
	//	fmt.Print(e)
	//}

	//username := os.Getenv("db_user")
	//password := os.Getenv("db_pass")
	//dbName := os.Getenv("db_name")
	//dbHost := os.Getenv("db_host")

	//TODO create configuration file, no static data should exist in the sourcecode
	var connectionStringTemplate = "mongodb://carpool:longlive0817@35.183.76.175:27017/carpool"
	clientOptions := options.Client().ApplyURI(connectionStringTemplate)
	client, err := mongo.Connect(context.TODO(), clientOptions)
	log.Println("Connecting to DB..")
	if err != nil {
		log.Println("Error occurred while connecting DB:")
		fmt.Print(err)
	}
	log.Println("Pinging DB..")
	err = client.Ping(context.TODO(), nil)
	if err != nil {
		log.Println("Error occurred while pinging DB:")
		fmt.Print(err)
	}
	db = client.Database("carpool")

	// _, err = db.Collection("User").InsertOne(nil, bson.M{"name": "pi", "value": 3.14159})
	// if err != nil {
	// 	fmt.Print(err)
	// }
}

//GetDB returns a handle to the DB client
func GetDB() *mongo.Database {
	return db
}
