package models

import (
	"context"
	"log"
	"shari/utils"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//FlightTrip structure defines flight trip information data
type FlightTrip struct {
	ID           primitive.ObjectID `json:"ID" bson:"_id"`
	OpenID       string
	FlightTime   time.Time
	Dep          string
	Dest         string
	Seat         int
	SmallLuggage int
	LargeLuggage int
}

//Create Flight trip data
func (ft *FlightTrip) Create() map[string]interface{} {
	//create db document
	log.Println("Creating Driver Trip with openid " + ft.OpenID)
	ft.ID = primitive.NewObjectID()
	_, err := GetDB().Collection("FlightTrip").InsertOne(nil, ft)
	if err != nil {
		log.Println(err)
	}
	response := utils.Message(true, "Driver Trip has been created")
	response["FlightTrip"] = ft
	return response
}

//GetFlightDriverFromForm returns driver list available for pickup/dropoff at the airport
func GetFlightDriverFromForm(ft *FlightTrip) []User {
	//pt is the request, trip is the data
	var driverList []User
	rawList, err := GetDB().Collection("User").Find(nil, bson.D{{Key: "flightdriver", Value: true}})
	if err != nil {
		log.Println(err)
		return nil
	}
	for rawList.Next(context.Background()) {
		driver := User{}
		err := rawList.Decode(&driver)
		if err != nil {
			//handle err
		}

		var isWayPoint bool
		for _, s := range driver.DestList {
			if s == ft.Dest || s == ft.Dep {
				isWayPoint = true
				break
			}
		}
		if isWayPoint {
			isWayPoint = false
			driverList = append(driverList, driver)
		}

	}
	return driverList
}

//UpdateFlightTrip update trip info
func UpdateFlightTrip(ft *FlightTrip) map[string]interface{} {
	result, err := GetDB().Collection("FlightTrip").UpdateOne(nil, bson.D{{Key: "_id", Value: ft.ID}}, bson.M{
		"$set": bson.M{
			"dep":          ft.Dep,
			"dest":         ft.Dest,
			"seat":         ft.Seat,
			"smallluggage": ft.SmallLuggage,
			"largeluggage": ft.LargeLuggage,
			"flighttime":   ft.FlightTime}})
	if err != nil {
		log.Println(err)
		return nil
	}
	response := utils.Message(false, "Trip not updated")
	if result.MatchedCount == 1 {
		response := utils.Message(true, "Flight Trip has been created")
		response["FlightTrip"] = ft
	}
	return response
}

//GetFlightTripFromUser returns trip list for flight drivers
func GetFlightTripFromUser(user *User) []FlightTrip {
	var tripList []FlightTrip
	rawList, err := GetDB().Collection("FlightTrip").Find(nil, bson.D{{}})
	if err != nil {
		log.Println(err)
		return nil
	}
	var i int
	for rawList.Next(context.Background()) {
		trip := FlightTrip{}
		err := rawList.Decode(&trip)
		if err != nil {
			//handle err
		}

		var isWayPoint bool
		for _, s := range user.DestList {
			if s == trip.Dest || s == trip.Dep {
				isWayPoint = true
				break
			}
		}
		if isWayPoint {
			isWayPoint = false
			tripList = append(tripList, trip)
			i++
		}

	}
	return tripList
}
