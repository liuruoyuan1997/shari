package models

import (
	"context"
	"log"
	"shari/utils"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//DriverTrip structure defines driver trip information data
type DriverTrip struct {
	ID           primitive.ObjectID `json:"Id" bson:"_id"`
	OpenID       string
	DepTimeStart time.Time
	DepTimeEnd   time.Time
	Dep          string
	Dest         string
	Seat         int
	LuggageOnly  bool
	WayPoint     []string
}

//Create driver trip
func (dt *DriverTrip) Create() map[string]interface{} {
	//create db document
	log.Println("Creating Driver Trip with openid " + dt.OpenID)
	dt.ID = primitive.NewObjectID()
	_, err := GetDB().Collection("DriverTrip").InsertOne(nil, dt)
	if err != nil {
		log.Println(err)
	}
	response := utils.Message(true, "Driver Trip has been created")
	response["DriverTrip"] = dt
	return response
}

//UpdateDriverTrip update trip info
func UpdateDriverTrip(dt *DriverTrip) map[string]interface{} {
	result, err := GetDB().Collection("DriverTrip").UpdateOne(nil, bson.D{{Key: "_id", Value: dt.ID}}, bson.M{
		"$set": bson.M{
			"dep":          dt.Dep,
			"dest":         dt.Dest,
			"seat":         dt.Seat,
			"luggageonly":  dt.LuggageOnly,
			"waypoint":     dt.WayPoint,
			"deptimestart": dt.DepTimeStart,
			"deptimeend":   dt.DepTimeEnd}})
	if err != nil {
		log.Println(err)
		return nil
	}
	response := utils.Message(false, "Trip not updated")
	if result.MatchedCount == 1 {
		response := utils.Message(true, "Driver Trip has been created")
		response["DriverTrip"] = dt
	}
	return response
}

//GetPassengerTripFromForm filters trip using form data
func GetPassengerTripFromForm(dt *DriverTrip) map[int]PassengerTrip {

	//pt is the request, trip is the data
	tripList := make(map[int]PassengerTrip)
	rawList, err := GetDB().Collection("PassengerTrip").Find(nil, bson.D{{Key: "dep", Value: dt.Dep}})
	if err != nil {
		log.Println(err)
		return nil
	}
	rawListDest, err := GetDB().Collection("PassengerTrip").Find(nil, bson.D{{Key: "dest", Value: dt.Dest}})
	if err != nil {
		log.Println(err)
		return nil
	}
	var i int
	for rawList.Next(context.Background()) {
		log.Println(rawList)
		trip := PassengerTrip{}
		err := rawList.Decode(&trip)
		if err != nil {
			//handle err
		}

		//luggage filter
		if dt.LuggageOnly != trip.LuggageOnly {
			continue
		}

		//seat filter
		if trip.Seat > dt.Seat {
			continue
		}

		//time filter
		if trip.DepTimeEnd.Before(dt.DepTimeStart) || trip.DepTimeStart.After(dt.DepTimeEnd) {
			continue
		}

		//waypoint filter
		var isWayPoint bool
		if trip.Dest != dt.Dest {
			for _, s := range dt.WayPoint {
				if s == trip.Dest {
					isWayPoint = true
					break
				}
			}
			if !isWayPoint {
				continue
			}
		} else {
			continue
		}

		tripList[i] = trip
		i++
	}

	for rawListDest.Next(context.Background()) {
		trip := PassengerTrip{}
		err := rawListDest.Decode(&trip)
		if err != nil {
			//handle err
		}

		//luggage filter
		if dt.LuggageOnly != trip.LuggageOnly {
			continue
		}

		//seat filter
		if trip.Seat > dt.Seat {
			continue
		}

		//time filter
		if trip.DepTimeEnd.Before(dt.DepTimeStart) || trip.DepTimeStart.After(dt.DepTimeEnd) {
			continue
		}

		//waypoint filter
		var isWayPoint bool
		if trip.Dep != dt.Dep {
			for _, s := range dt.WayPoint {
				if s == trip.Dep {
					isWayPoint = true
					break
				}
			}
			if !isWayPoint {
				continue
			}
		}

		tripList[i] = trip
		i++
	}

	log.Println(tripList)
	return tripList
}
