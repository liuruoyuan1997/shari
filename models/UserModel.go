package models

import (
	"log"
	"shari/utils"

	"go.mongodb.org/mongo-driver/mongo"

	"go.mongodb.org/mongo-driver/bson"
)

//User data structure defines user information data
type User struct {
	OpenID       string
	PhoneNum     string
	FlightDriver bool
	SmallLuggage int
	LargeLuggage int
	DestList     []string
	Comment      string
	VehicleType  string
}

//Create User creates default user document in the database. Returns a response
func (user *User) Create() map[string]interface{} {
	_, err := GetDB().Collection("User").InsertOne(nil, user)
	if err != nil {
		log.Println("Error while creating user:")
		log.Println(err)
		response := utils.Message(false, "User Creation Error")
		response["Error"] = err
		return response
	}
	response := utils.Message(true, "User Creation Success")
	response["User"] = user
	return response
}

//GetUser return the user data from ID
func GetUser(OpenID string) *User {
	user := &User{}
	log.Println("Retriving user data:" + OpenID)
	err := GetDB().Collection("User").FindOne(nil, bson.D{{Key: "openid", Value: OpenID}}).Decode(user)
	if err == nil {
		log.Println("Error while decoding user")
		log.Println(err)
	}
	if user == nil {
		log.Println("User not found for" + OpenID)
	} else {
		log.Println("User Found")
		log.Println(user)
	}
	return user
}

//GetPhoneNum return the user phone number from ID
func GetPhoneNum(ID string) string {
	user := GetUser(ID)
	return user.PhoneNum
}

//UpdatePhoneNum updates user's phone number in the database.
func UpdatePhoneNum(ID string, phoneNum string) bool {
	log.Println("Updating user PhoneNum:" + ID)
	user := &User{}
	err := GetDB().Collection("User").FindOne(nil, bson.D{{Key: "openid", Value: ID}}).Decode(user)
	if user.PhoneNum == phoneNum {
		return true
	}

	resp, err := GetDB().Collection("User").UpdateOne(nil, bson.D{{Key: "openid", Value: ID}}, bson.M{"$set": bson.M{"phonenum": phoneNum}})
	if err != nil {
		log.Println("Error while updating PhoneNum" + ID)
		log.Println(err)
		return false
	}

	if resp.MatchedCount != 1 || resp.MatchedCount != resp.ModifiedCount {
		log.Println("Error while updating PhoneNum" + ID)
		log.Println(resp)
		return false
	}
	return true
}

//UpdateIdentity updates user's identity(is flight driver or not) in the database.
func UpdateIdentity(ID string, identity bool) *mongo.UpdateResult {
	resp, err := GetDB().Collection("User").UpdateOne(nil, bson.D{{Key: "openid", Value: ID}}, bson.M{"$set": bson.M{"flightdriver": identity}})
	if err != nil {
		log.Println(err)
		return nil
	}
	return resp
}

//UpdateVehicleType updates user's vehicle type in the database.
func UpdateVehicleType(ID string, vehicleType string) *mongo.UpdateResult {
	resp, err := GetDB().Collection("User").UpdateOne(nil, bson.D{{Key: "openid", Value: ID}}, bson.M{"$set": bson.M{"vehicletype": vehicleType}})
	if err != nil {
		log.Println(err)
		return nil
	}
	return resp
}

//UpdateSmallLuggage updates user's small luggage number in the database.
func UpdateSmallLuggage(ID string, luggageNum int) *mongo.UpdateResult {
	resp, err := GetDB().Collection("User").UpdateOne(nil, bson.D{{Key: "openid", Value: ID}}, bson.M{"$set": bson.M{"smallluggage": luggageNum}})
	if err != nil {
		log.Println(err)
		return nil
	}
	return resp
}

//UpdateLargeLuggage updates user's large luggage number in the database.
func UpdateLargeLuggage(ID string, luggageNum int) *mongo.UpdateResult {
	resp, err := GetDB().Collection("User").UpdateOne(nil, bson.D{{Key: "openid", Value: ID}}, bson.M{"$set": bson.M{"largeluggage": luggageNum}})
	if err != nil {
		log.Println(err)
		return nil
	}
	return resp
}

//UpdateComment updates user's comment in the database.
func UpdateComment(ID string, comment string) *mongo.UpdateResult {
	resp, err := GetDB().Collection("User").UpdateOne(nil, bson.D{{Key: "openid", Value: ID}}, bson.M{"$set": bson.M{"comment": comment}})
	if err != nil {
		log.Println(err)
		return nil
	}
	return resp
}

//UpdateDestList updates user's destination list in the database.
func UpdateDestList(ID string, destList []string) *mongo.UpdateResult {
	resp, err := GetDB().Collection("User").UpdateOne(nil, bson.D{{Key: "openid", Value: ID}}, bson.M{"$set": bson.M{"destlist": destList}})
	if err != nil {
		log.Println(err)
		return nil
	}
	return resp
}
