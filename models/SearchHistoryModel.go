package models

import (
	"context"
	"log"
	"shari/utils"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//SearchHistory struc
type SearchHistory struct {
	OpenID       string
	CreationTime time.Time
	ID           primitive.ObjectID `json:"ID" bson:"_id"`
	Type         string
	//TODO
}

//Create search history document in database
func (sh *SearchHistory) Create() map[string]interface{} {
	_, err := GetDB().Collection("SearchHistory").InsertOne(nil, sh)
	if err != nil {
		log.Println("Error while creating search history:")
		log.Println(err)
		response := utils.Message(false, "Search History Creation Error")
		response["Error"] = err
		return response
	}
	response := utils.Message(true, "Search history has been created")
	response["search history"] = sh
	return response
}

//GetSearchHistoryList gets search history in database.
func GetSearchHistoryList(OpenID string) []SearchHistory {
	searchHistoryList := []SearchHistory{}
	rawList, err := GetDB().Collection("SearchHistory").Find(nil, bson.D{{Key: "openid", Value: OpenID}})
	if err != nil {
		log.Println(err)
		return nil
	}
	for rawList.Next(context.Background()) {
		sh := SearchHistory{}
		rawList.Decode(&sh)
		searchHistoryList = append(searchHistoryList, sh)
	}

	return searchHistoryList
}

//GetSearchHistoryDetail gets search history detail(trips) in database.
func GetSearchHistoryDetail(sh SearchHistory) map[string]interface{} {
	var trip map[string]interface{}
	GetDB().Collection(sh.Type+"Trip").FindOne(nil, bson.D{{Key: "_id", Value: sh.ID}}).Decode(&trip)
	return trip
}
