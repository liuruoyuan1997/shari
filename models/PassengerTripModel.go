package models

import (
	"context"
	"log"
	"shari/utils"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//PassengerTrip structure defines passenger trip information data
type PassengerTrip struct {
	ID           primitive.ObjectID `json:"ID" bson:"_id"`
	OpenID       string
	DepTimeStart time.Time
	DepTimeEnd   time.Time
	Dep          string
	Dest         string
	Seat         int
	LuggageOnly  bool
}

//Create Passenger trip
func (pt *PassengerTrip) Create() map[string]interface{} {
	//create db document
	log.Println("Creating user with openid " + pt.OpenID)
	pt.ID = primitive.NewObjectID()
	insRes, err := GetDB().Collection("PassengerTrip").InsertOne(nil, pt)
	if err != nil {
		log.Println(err)
	}
	response := utils.Message(true, "Passenger Trip has been created")
	response["PassengerTrip"] = pt
	response["PassengerTripID"] = insRes.InsertedID.(primitive.ObjectID)
	return response
}

//UpdatePassengerTrip update trip info
func UpdatePassengerTrip(pt *PassengerTrip) map[string]interface{} {
	result, err := GetDB().Collection("PassengerTrip").UpdateOne(nil, bson.D{{Key: "_id", Value: pt.ID}}, bson.M{
		"$set": bson.M{
			"dep":          pt.Dep,
			"dest":         pt.Dest,
			"seat":         pt.Seat,
			"luggageonly":  pt.LuggageOnly,
			"deptimestart": pt.DepTimeStart,
			"deptimeend":   pt.DepTimeEnd}})

	if err != nil {
		log.Println(err)
		return nil
	}

	response := utils.Message(false, "Trip not updated")
	if result.MatchedCount == 1 {
		response = utils.Message(true, "Passenger Trip has been created")
	}
	response["PassengerTrip"] = pt
	return response
}

//GetDriverTripFromForm filters trip from form data
func GetDriverTripFromForm(pt *PassengerTrip) map[int]DriverTrip {
	//pt is the request, trip is the data
	tripList := make(map[int]DriverTrip)
	rawList, err := GetDB().Collection("DriverTrip").Find(nil, bson.D{{Key: "dep", Value: pt.Dep}})
	if err != nil {
		log.Println(err)
		return nil
	}

	rawListDest, err := GetDB().Collection("DriverTrip").Find(nil, bson.D{{Key: "dest", Value: pt.Dest}})
	if err != nil {
		log.Println(err)
		return nil
	}

	var i int
	for rawList.Next(context.Background()) {
		trip := DriverTrip{}
		err := rawList.Decode(&trip)
		if err != nil {
			//handle err
		}

		//luggage filter
		if pt.LuggageOnly != trip.LuggageOnly {
			continue
		}

		//seat filter
		if trip.Seat < pt.Seat {
			continue
		}

		//time filter
		if trip.DepTimeEnd.Before(pt.DepTimeStart) || trip.DepTimeStart.After(pt.DepTimeEnd) {
			continue
		}

		//waypoint filter
		var isWayPoint bool
		if trip.Dest != pt.Dest {
			for _, s := range trip.WayPoint {
				if s == pt.Dest {
					isWayPoint = true
					break
				}
			}
			if !isWayPoint {
				continue
			}
		} else {
			//ignore absolute match first search.
			continue
		}

		tripList[i] = trip
		i++
	}

	for rawListDest.Next(context.Background()) {
		trip := DriverTrip{}
		err := rawListDest.Decode(&trip)
		if err != nil {
			//handle err
		}

		//luggage filter
		if pt.LuggageOnly != trip.LuggageOnly {
			continue
		}

		//seat filter
		if trip.Seat < pt.Seat {
			continue
		}

		//time filter
		if trip.DepTimeEnd.Before(pt.DepTimeStart) || trip.DepTimeStart.After(pt.DepTimeEnd) {
			continue
		}

		//waypoint filter
		var isWayPoint bool
		if trip.Dep != pt.Dep {
			for _, s := range trip.WayPoint {
				if s == pt.Dep {
					isWayPoint = true
					break
				}
			}
			if !isWayPoint {
				continue
			}
		} else {
			//ignore absolute match first search.
			continue
		}

		tripList[i] = trip
		i++
	}
	//TODO filter trip destination and way point
	//TODO filter trip time
	//TODO filter trip seat
	return tripList
}
