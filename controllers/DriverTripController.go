package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"shari/models"
	"shari/utils"
)

//CreateDriverTrip saves Trip data from drivers to database.
var CreateDriverTrip = func(w http.ResponseWriter, r *http.Request) {

	dt := &models.DriverTrip{}
	err := json.NewDecoder(r.Body).Decode(dt) //decode the request body into struct and failed if any error occur
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		log.Println(err)
		return
	}

	resp := dt.Create() //Create account
	utils.Respond(w, resp)
}

//GetPassengerTripInstaMatch gets filtered trip for driver
var GetPassengerTripInstaMatch = func(w http.ResponseWriter, r *http.Request) {
	dt := &models.DriverTrip{}
	err := json.NewDecoder(r.Body).Decode(dt)
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}

	tripList := models.GetPassengerTripFromForm(dt)
	resp := utils.Message(true, "success")
	resp["tripList"] = tripList
	utils.Respond(w, resp)
}

//UpdateDriverTrip updates driver trip using trip id
var UpdateDriverTrip = func(w http.ResponseWriter, r *http.Request) {
	trip := &models.DriverTrip{}
	err := json.NewDecoder(r.Body).Decode(trip) //decode the request body into struct and failed if any error occur
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}
	resp := models.UpdateDriverTrip(trip)
	utils.Respond(w, resp)
}
