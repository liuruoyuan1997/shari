package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"shari/models"
	"shari/utils"
)

//CreateSearchHistory saves search history data to the database
var CreateSearchHistory = func(w http.ResponseWriter, r *http.Request) {

	sh := &models.SearchHistory{}
	err := json.NewDecoder(r.Body).Decode(sh)
	if err != nil {
		log.Println("Error while decoding search history:")
		log.Println(err)
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}
	log.Println("Creating search history:")
	log.Println(sh)
	resp := sh.Create() //Create account
	utils.Respond(w, resp)
}

//GetSearchHistoryList gets search history using user open id
var GetSearchHistoryList = func(w http.ResponseWriter, r *http.Request) {
	var user models.User
	json.NewDecoder(r.Body).Decode(&user)
	shList := models.GetSearchHistoryList(user.OpenID)
	var resp map[string]interface{}
	if len(shList) == 0 {
		resp = utils.Message(true, "No valid Search History")
	} else {
		resp = utils.Message(true, "success")
	}
	resp["SearchHistory"] = shList
	utils.Respond(w, resp)
}

//GetSearchHistoryDetail gets search history details using id and trip type
var GetSearchHistoryDetail = func(w http.ResponseWriter, r *http.Request) {
	sh := models.SearchHistory{}
	json.NewDecoder(r.Body).Decode(&sh)
	trip := models.GetSearchHistoryDetail(sh)
	resp := utils.Message(true, "success")
	resp["SearchHistoryDetail"] = trip
	utils.Respond(w, resp)
}
