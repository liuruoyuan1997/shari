package controllers

import (
	"encoding/json"
	"log"
	"net/http"
	"shari/models"
	"shari/utils"
)

//CreateUser saves user data to database.
var CreateUser = func(w http.ResponseWriter, r *http.Request) {

	user := &models.User{}
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		log.Println("Error when decode json to user:")
		log.Println(err)
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}
	log.Println("Creating user:")
	log.Println(user)
	resp := user.Create()
	utils.Respond(w, resp)
}

//GetUser gets user using user open id
var GetUser = func(w http.ResponseWriter, r *http.Request) {
	var param map[string]interface{}
	json.NewDecoder(r.Body).Decode(&param)
	user := models.GetUser(param["OpenId"].(string))
	if user == nil {
		resp := utils.Message(false, "User not found")
		utils.Respond(w, resp)
	}
	resp := utils.Message(true, "success")
	resp["User"] = user
	utils.Respond(w, resp)
}

//GetPhoneNum gets phone num using user open id
var GetPhoneNum = func(w http.ResponseWriter, r *http.Request) {
	var param map[string]interface{}
	json.NewDecoder(r.Body).Decode(&param)
	phoneNum := models.GetPhoneNum(param["OpenId"].(string))
	if phoneNum == "" {
		resp := utils.Message(false, "PhoneNum not found")
		utils.Respond(w, resp)
	}
	resp := utils.Message(true, "success")
	resp["PhoneNum"] = phoneNum
	utils.Respond(w, resp)
}

//UpdatePhoneNum updates phone num using user open id
var UpdatePhoneNum = func(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(&user)
	log.Println("Updating phone num" + user.OpenID)
	result := models.UpdatePhoneNum(user.OpenID, user.PhoneNum)
	resp := utils.Message(true, "success")
	if result == false {
		resp = utils.Message(false, "Phone Num not updated")
	}
	resp["UpdateResult"] = result
	utils.Respond(w, resp)
}

//UpdateIdentity updates user identity using user open id
var UpdateIdentity = func(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(&user)
	result := models.UpdateIdentity(user.OpenID, user.FlightDriver)
	resp := utils.Message(true, "success")
	resp["UpdateResult"] = result
	utils.Respond(w, resp)
}

//UpdateVehicleType updates vahicle type using user open id
var UpdateVehicleType = func(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(&user)
	result := models.UpdateVehicleType(user.OpenID, user.VehicleType)
	resp := utils.Message(true, "success")
	resp["UpdateResult"] = result
	utils.Respond(w, resp)
}

//UpdateSmallLuggage updates small luggage using user open id
var UpdateSmallLuggage = func(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(&user)
	result := models.UpdateSmallLuggage(user.OpenID, user.SmallLuggage)
	resp := utils.Message(true, "success")
	resp["UpdateResult"] = result
	utils.Respond(w, resp)
}

//UpdateLargeLuggage updates large luggage using user open id
var UpdateLargeLuggage = func(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(&user)
	result := models.UpdateLargeLuggage(user.OpenID, user.LargeLuggage)
	resp := utils.Message(true, "success")
	resp["UpdateResult"] = result
	utils.Respond(w, resp)
}

//UpdateComment updates comment using user open id
var UpdateComment = func(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(&user)
	result := models.UpdateComment(user.OpenID, user.Comment)
	resp := utils.Message(true, "success")
	resp["UpdateResult"] = result
	utils.Respond(w, resp)
}

//UpdateDestList updates destination list using user open id
var UpdateDestList = func(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	json.NewDecoder(r.Body).Decode(&user)
	result := models.UpdateDestList(user.OpenID, user.DestList)
	resp := utils.Message(true, "success")
	resp["UpdateResult"] = result
	utils.Respond(w, resp)
}
