package controllers

import (
	"encoding/json"
	"net/http"
	"shari/models"
	"shari/utils"
)

//CreatePassengerTrip saves trip data from passenger to database.
var CreatePassengerTrip = func(w http.ResponseWriter, r *http.Request) {

	pt := &models.PassengerTrip{}
	err := json.NewDecoder(r.Body).Decode(pt) //decode the request body into struct and failed if any error occur
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}

	resp := pt.Create() //Create account
	utils.Respond(w, resp)
}

//GetDriverTripInstaMatch gets filtered trip for passenger
var GetDriverTripInstaMatch = func(w http.ResponseWriter, r *http.Request) {
	pt := &models.PassengerTrip{}
	err := json.NewDecoder(r.Body).Decode(pt)
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}

	tripList := models.GetDriverTripFromForm(pt)
	resp := utils.Message(true, "success")
	resp["tripList"] = tripList
	utils.Respond(w, resp)
}

//UpdatePassengerTrip updates driver trip using trip id
var UpdatePassengerTrip = func(w http.ResponseWriter, r *http.Request) {
	trip := &models.PassengerTrip{}
	err := json.NewDecoder(r.Body).Decode(trip) //decode the request body into struct and failed if any error occur
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}
	resp := models.UpdatePassengerTrip(trip)
	utils.Respond(w, resp)
}
