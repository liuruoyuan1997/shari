package controllers

import (
	"encoding/json"
	"net/http"
	"shari/models"
	"shari/utils"
)

//CreateFlightTrip saves trip data from flight passenger to database.
var CreateFlightTrip = func(w http.ResponseWriter, r *http.Request) {

	ft := &models.FlightTrip{}
	err := json.NewDecoder(r.Body).Decode(ft) //decode the request body into struct and failed if any error occur
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}

	resp := ft.Create() //Create account
	utils.Respond(w, resp)
}

//GetFlightDriverInstaMatch gets available driver list for flight trips
var GetFlightDriverInstaMatch = func(w http.ResponseWriter, r *http.Request) {
	ft := &models.FlightTrip{}
	err := json.NewDecoder(r.Body).Decode(ft)
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}

	driverList := models.GetFlightDriverFromForm(ft)
	resp := utils.Message(true, "success")
	resp["DriverList"] = driverList
	utils.Respond(w, resp)
}

//GetFlightTripInstaMatch gets available driver list for flight trips
var GetFlightTripInstaMatch = func(w http.ResponseWriter, r *http.Request) {
	user := &models.User{}
	err := json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}

	tripList := models.GetFlightTripFromUser(user)
	resp := utils.Message(true, "success")
	resp["TripList"] = tripList
	utils.Respond(w, resp)
}

//UpdateFlightTrip updates driver trip using trip id
var UpdateFlightTrip = func(w http.ResponseWriter, r *http.Request) {
	trip := &models.FlightTrip{}
	err := json.NewDecoder(r.Body).Decode(trip) //decode the request body into struct and failed if any error occur
	if err != nil {
		utils.Respond(w, utils.Message(false, "Invalid request"))
		return
	}
	resp := models.UpdateFlightTrip(trip)
	utils.Respond(w, resp)
}
