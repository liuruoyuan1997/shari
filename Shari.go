package main

import (
	"fmt"
	"net/http"
	"os"
	"shari/controllers"

	"github.com/gorilla/mux"
)

func main() {
	//router := mux.NewRouter()
	router := mux.NewRouter()
	//router.Use(app.JwtAuthentication) //attach JWT auth middleware

	//POST apis*********************
	router.HandleFunc("/api/user/new", controllers.CreateUser).Methods("POST")
	router.HandleFunc("/api/user/updatephonenum", controllers.UpdatePhoneNum).Methods("POST")
	router.HandleFunc("/api/user/updateidentity", controllers.UpdateIdentity).Methods("POST")
	router.HandleFunc("/api/user/updatedestlist", controllers.UpdateDestList).Methods("POST")
	router.HandleFunc("/api/user/updatelargeluggage", controllers.UpdateLargeLuggage).Methods("POST")
	router.HandleFunc("/api/user/updatesmallluggage", controllers.UpdateSmallLuggage).Methods("POST")
	router.HandleFunc("/api/user/updatecomment", controllers.UpdateComment).Methods("POST")
	router.HandleFunc("/api/user/updatevehicletype", controllers.UpdateVehicleType).Methods("POST")

	router.HandleFunc("/api/driver/newdrivertrip", controllers.CreateDriverTrip).Methods("POST")
	router.HandleFunc("/api/driver/updatedrivertrip", controllers.UpdateDriverTrip).Methods("POST")
	router.HandleFunc("/api/passenger/newpassengertrip", controllers.CreatePassengerTrip).Methods("POST")
	router.HandleFunc("/api/passenger/updatepassengertrip", controllers.UpdatePassengerTrip).Methods("POST")
	router.HandleFunc("/api/flight/newflighttrip", controllers.CreateFlightTrip).Methods("POST")
	router.HandleFunc("/api/flight/updateflighttrip", controllers.UpdateFlightTrip).Methods("POST")
	router.HandleFunc("/api/searchhistory/new", controllers.CreateSearchHistory).Methods("POST")

	//GET apis**********************
	router.HandleFunc("/api/user/getphonenum", controllers.GetPhoneNum).Methods("GET")
	router.HandleFunc("/api/user/getuser", controllers.GetUser).Methods("GET")
	router.HandleFunc("/api/passenger/gettriplist", controllers.GetDriverTripInstaMatch).Methods("GET")
	router.HandleFunc("/api/driver/gettriplist", controllers.GetPassengerTripInstaMatch).Methods("GET")
	router.HandleFunc("/api/flight/gettriplist", controllers.GetFlightTripInstaMatch).Methods("GET")
	router.HandleFunc("/api/flight/getdriverlist", controllers.GetFlightDriverInstaMatch).Methods("GET")
	router.HandleFunc("/api/searchhistory/getsearchhistorylist", controllers.GetSearchHistoryList).Methods("GET")
	router.HandleFunc("/api/searchhistory/getsearchhistorydetail", controllers.GetSearchHistoryDetail).Methods("GET")

	port := os.Getenv("PORT") //Get port from .env file, we did not specify any port so this should return an empty string when tested locally
	if port == "" {
		port = "8000" //localhost
	}
	fmt.Println(port)
	err := http.ListenAndServe(":"+port, router) //Launch the app, visit localhost:8000/api
	if err != nil {
		fmt.Print(err)
	}
}
